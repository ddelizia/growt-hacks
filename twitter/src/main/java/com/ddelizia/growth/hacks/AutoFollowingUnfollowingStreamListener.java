package com.ddelizia.growth.hacks;

import com.optimaize.langdetect.LanguageDetector;
import org.apache.log4j.Logger;
import twitter4j.*;

import java.util.Locale;
import java.util.Map;

/**
 * Created by ddelizia on 11/03/15.
 */
public class AutoFollowingUnfollowingStreamListener implements UserStreamListener{

    private static final org.apache.log4j.Logger LOGGER = Logger.getLogger(AutoFollowingUnfollowingStreamListener.class);

    private  Twitter twitter;

    private boolean production;

    private Map<String, String> directMessageResponses;

    private String userScreenName;

    private String defaultLocale;


    public AutoFollowingUnfollowingStreamListener(final Twitter twitter, final boolean production, final Map<String, String> directMessageResponses, final String userScreenName, final String defaultLocale) {
        this.twitter = twitter;
        this.production = production;
        this.directMessageResponses = directMessageResponses;
        this.userScreenName = userScreenName;
        this.defaultLocale = defaultLocale;
    }

    @Override
    public void onException(Exception e) {

    }

    @Override
    public void onStatus(Status status) {

    }

    @Override
    public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {

    }

    @Override
    public void onTrackLimitationNotice(int i) {

    }

    @Override
    public void onScrubGeo(long l, long l1) {

    }

    @Override
    public void onStallWarning(StallWarning stallWarning) {

    }

    @Override
    public void onDeletionNotice(long l, long l1) {

    }

    @Override
    public void onFriendList(long[] longs) {

    }

    @Override
    public void onFavorite(User user, User user1, Status status) {

    }

    @Override
    public void onUnfavorite(User user, User user1, Status status) {

    }

    @Override
    public void onFollow(User user, User user1) {
        LOGGER.info(String.format("User %s is following %s", user.getScreenName(), user1.getScreenName()));
        if (user1.getScreenName().equals(userScreenName)){

            String message = directMessageResponses.get(defaultLocale);
            if (user.getLang()!=null){
                message = directMessageResponses.get(user.getLang());

            }
            LOGGER.info(String.format("User %s is following %s. Sending Message based on language detection: %s", user.getScreenName(), user1.getScreenName(), message));

            if (production) {
                try {
                    twitter.createFriendship(user.getScreenName());
                    twitter.sendDirectMessage(user.getScreenName(), message);
                } catch (TwitterException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onUnfollow(User user, User user1) {
        LOGGER.info(String.format("User %s is unfollowing %s", user.getScreenName(), user1.getScreenName()));

        if (user1.getScreenName().equals(userScreenName)){

            LOGGER.info(String.format("User %s is not following anymore %s.", user.getScreenName(), user1.getScreenName()));

            if (production) {
                try {
                    twitter.destroyFriendship(user.getScreenName());
                } catch (TwitterException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onDirectMessage(DirectMessage directMessage) {

    }

    @Override
    public void onUserListMemberAddition(User user, User user1, UserList userList) {

    }

    @Override
    public void onUserListMemberDeletion(User user, User user1, UserList userList) {

    }

    @Override
    public void onUserListSubscription(User user, User user1, UserList userList) {

    }

    @Override
    public void onUserListUnsubscription(User user, User user1, UserList userList) {

    }

    @Override
    public void onUserListCreation(User user, UserList userList) {

    }

    @Override
    public void onUserListUpdate(User user, UserList userList) {

    }

    @Override
    public void onUserListDeletion(User user, UserList userList) {

    }

    @Override
    public void onUserProfileUpdate(User user) {

    }

    @Override
    public void onUserSuspension(long l) {

    }

    @Override
    public void onUserDeletion(long l) {

    }

    @Override
    public void onBlock(User user, User user1) {

    }

    @Override
    public void onUnblock(User user, User user1) {

    }

}
