package com.ddelizia.growth.hacks;

import com.google.common.base.Preconditions;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import twitter4j.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @author ddelizia
 * @since 17/04/15 15:16
 */
public class RetweetInterestingListener implements  StatusListener{

    private static final Logger LOGGER = Logger.getLogger(RetweetInterestingListener.class);

    private Twitter twitter;

    private MongoClient mongoClient;

    private String db;

    private String collection;

    private DBCollection dbCollection;

    private boolean production = false;

    private List<String> languages;

    private int minimalRetweets;

    public RetweetInterestingListener(final Twitter twitter, final MongoClient mongoClient, final String db, final String collection, final boolean production, final List<String> languages, final int minimalRetweets) {
        this.twitter = twitter;
        this.mongoClient = mongoClient;
        this.db = db;
        this.collection = collection;
        this.production = production;
        this.languages = languages;
        this.minimalRetweets = minimalRetweets;

        this.dbCollection = this.mongoClient.getDB(this.db).getCollection(this.collection);
    }

    @Override
    public void onStatus(final Status status) {
        LOGGER.info("Number of reteets: "+ status.getRetweetCount() +" - Status retweetted: " + status.getRetweetedStatus().getText());

        if (status.getRetweetedStatus()!=null){
            LOGGER.info(" " + status.getRetweetedStatus().getRetweetCount());
        }

        if ((minimalRetweets == 0 && status.getRetweetedStatus()==null) ||
                (status.getRetweetedStatus()!=null && minimalRetweets==status.getRetweetedStatus().getRetweetCount())) {
            LOGGER.info("Value of retweet: "+status.getRetweetedStatus().getText());

            if (languages.contains(status.getLang()) || languages == null) {
                for (URLEntity urlEntity : status.getURLEntities()) {

                    LOGGER.info("Launguage: " + status.getLang() + " - Original url: " + urlEntity.getExpandedURL());
                    String unshortenedUrl = null;
                    try {
                        unshortenedUrl = expand(urlEntity.getExpandedURL());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    LOGGER.info("Final link: " + unshortenedUrl);

                    if (unshortenedUrl != null) {

                        BasicDBObject query = new BasicDBObject("url", unshortenedUrl);

                        if (dbCollection.count(query) <= 0) {
                            LOGGER.info(unshortenedUrl + " - No in db so retweeting");
                            //Retweet and push to document
                            try {
                                if (production) {
                                    if (minimalRetweets > 0){
                                        twitter.retweetStatus(status.getRetweetedStatus().getId());
                                    }else {
                                        twitter.retweetStatus(status.getId());
                                    }
                                }

                                dbCollection.save(query);
                            } catch (TwitterException e) {
                                e.printStackTrace();
                            }
                        } else {
                            LOGGER.info(unshortenedUrl + " - Found in db");
                        }
                    } else {
                        LOGGER.error("URL == null");
                    }
                }
            }
        }

    }

    @Override
    public void onDeletionNotice(final StatusDeletionNotice statusDeletionNotice) {

    }

    @Override
    public void onTrackLimitationNotice(final int i) {

    }

    @Override
    public void onScrubGeo(final long l, final long l1) {

    }

    @Override
    public void onStallWarning(final StallWarning stallWarning) {

    }

    @Override
    public void onException(final Exception e) {

    }


    public static String expandSingleLevel(String url) throws IOException {
        HttpParams httpParameters = new BasicHttpParams();
        httpParameters.setParameter("http.protocol.handle-redirects", false);
        DefaultHttpClient client = new DefaultHttpClient(httpParameters);

        HttpGet request = null;
        HttpEntity httpEntity = null;
        InputStream entityContentStream = null;

        try {
            request = new HttpGet(url);
            org.apache.http.HttpResponse httpResponse = client.execute(request);

            httpEntity = httpResponse.getEntity();
            entityContentStream = httpEntity.getContent();

            int statusCode = httpResponse.getStatusLine().getStatusCode();
            if (statusCode != 301 && statusCode != 302) {
                return url;
            }
            Header[] headers = httpResponse.getHeaders(HttpHeaders.LOCATION);
            Preconditions.checkState(headers.length == 1);
            String newUrl = headers[0].getValue();

            return newUrl;
        } catch (IllegalArgumentException uriEx) {
            return url;
        } finally {
            if (request != null) {
                request.releaseConnection();
            }
            if (entityContentStream != null) {
                entityContentStream.close();
            }
            if (httpEntity != null) {
                EntityUtils.consume(httpEntity);
            }
        }
    }

    public static String expand(String urlArg) throws IOException {
        String originalUrl = urlArg;
        String newUrl = expandSingleLevel(originalUrl);
        while (!originalUrl.equals(newUrl)) {
            originalUrl = newUrl;
            newUrl = expandSingleLevel(originalUrl);
        }
        return newUrl;
    }
}
