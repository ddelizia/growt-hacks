package com.ddelizia.growth.hacks;



import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import twitter4j.*;

import java.util.Map;
import java.util.Random;

/**
 * Created by ddelizia on 11/03/15.
 */
public class AutoResponderStreamListener implements StatusListener {

    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(AutoResponderStreamListener.class);

    private boolean production = false;

    private Twitter twitter;

    private String[] statusPlaceholder;

    private String regexListener;

    private Map<String,String> urlChoice;

    private String defaultUrl;

    private MongoClient mongoClient;

    private String db;

    private String collection;

    private DBCollection dbCollection;

    public AutoResponderStreamListener(final boolean production, final Twitter twitter, final String[] statusPlaceholder, final String regexListener, final Map<String, String> urlChoice, final String defaultUrl, final MongoClient mongoClient, final String db, final String collection) {
        this.production = production;
        this.twitter = twitter;
        this.statusPlaceholder = statusPlaceholder;
        this.regexListener = regexListener;
        this.urlChoice = urlChoice;
        this.defaultUrl = defaultUrl;
        this.mongoClient = mongoClient;
        this.db = db;
        this.collection = collection;

        this.dbCollection = this.mongoClient.getDB(this.db).getCollection(this.collection);
    }

    @Override
    public void onStatus(Status status) {

        BasicDBObject query = new BasicDBObject("contactedUserId", status.getUser().getId());

        if (status.getRetweetedStatus() == null && dbCollection.count(query) <= 0) {

            dbCollection.save(query);

            LOGGER.info(String.format("User is talking about %s: @%s - %s", regexListener, status.getUser().getScreenName(), status.getText()));

            StatusUpdate statusUpdate = new StatusUpdate(createStatus(status.getText(), status.getUser().getScreenName()));
            statusUpdate.setInReplyToStatusId(status.getId());

            LOGGER.info("Will send message: " + statusUpdate.getStatus());

            if (production) {
                try {
                    twitter.updateStatus(statusUpdate);
                } catch (TwitterException e) {
                    e.printStackTrace();
                }
            }
        }else{
            LOGGER.info(status.getUser() + " has been already contacted. Skipping");
        }

    }

    @Override
    public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {

    }

    @Override
    public void onTrackLimitationNotice(int i) {

    }

    @Override
    public void onScrubGeo(long l, long l1) {

    }

    @Override
    public void onStallWarning(StallWarning stallWarning) {

    }

    @Override
    public void onException(Exception e) {

    }

    private String selectString(){
        Random rand = new Random();
        int randomNum = rand.nextInt((statusPlaceholder.length - 0) + 1) + 0;

        return statusPlaceholder[randomNum];
    }

    private String createStatus(String fromStatus, String user){
        for (String regex: urlChoice.keySet()){
            if (fromStatus.matches(regex)){
                return String.format(selectString(), user, urlChoice.get(regex) );
            }
        }
        return String.format(selectString(), user, defaultUrl );
    }
}
