package com.ddelizia.growth.hacks;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by DaniloDeliziaICFI on 03/08/15.
 */
public class RetweetInterestingListenerTest {

    @Test
    public void testExpandSingleLevel() throws Exception {

        String result = RetweetInterestingListener.expandSingleLevel("http://t.co/IgdUtBafOU");
        Assert.assertEquals(result, "http://buff.ly/1SyxS4M");
    }

    @Test
    public void testExpand() throws Exception {

        String result = RetweetInterestingListener.expand("http://t.co/IgdUtBafOU");
        Assert.assertEquals(result, "https://egghead.io/articles/gentle-introduction-to-the-react-flux-architecture?utm_content=buffer85e4a&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer");
    }
}