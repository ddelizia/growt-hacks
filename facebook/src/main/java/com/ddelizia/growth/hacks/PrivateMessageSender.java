package com.ddelizia.growth.hacks;

import facebook4j.Facebook;
import facebook4j.FacebookException;
import facebook4j.Friend;
import facebook4j.ResponseList;
import org.apache.commons.beanutils.BeanUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.stringtemplate.v4.ST;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by ddelizia on 19/03/15.
 */
public class PrivateMessageSender {

    private Facebook facebook;

    private Map<Locale, String> messageTemplates;
    
    private List<String> friendProperties;

    private boolean production;

    public PrivateMessageSender(Facebook facebook, Map<Locale, String> messageTemplates, List<String> friendProperties, boolean production) {
        this.facebook = facebook;
        this.messageTemplates = messageTemplates;
        this.friendProperties = friendProperties;
        this.production = production;
    }

    public void send() throws FacebookException {
        WebDriver driver = new FirefoxDriver();

        // Go to the Google Suggest home page
        driver.get("https://www.facebook.com/");

        WebElement email = driver.findElement(By.id("email"));
        email.sendKeys("danilodelizia@yahoo.it");

        WebElement pass = driver.findElement(By.id("pass"));
        pass.sendKeys("Maverick1984");

        WebElement button = driver.findElement(By.id("u_0_l"));
        button.submit();

        ResponseList<Friend> responseList = facebook.friends().getFriends();
        for(Friend friend: responseList){
            String username = friend.getUsername();

            String messageTemplate = messageTemplates.get(friend.getLocale());

            if(messageTemplate != null) {

                driver.get("https://www.facebook.com/messages/" + username);

                ST st = new ST(messageTemplate);
                for (String friendProperty : friendProperties) {
                    try {
                        st.add(friendProperty, BeanUtils.getProperty(friend, friendProperty));
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    }
                }

                WebElement message = driver.findElement(By.name("message_body"));
                message.sendKeys(st.render());

                WebElement uncheck = driver.findElement(By.cssSelector("span._1s0"));
                boolean isSelected = uncheck.isSelected();
                if (isSelected) {
                    uncheck.click();
                }

                if (!production) {
                    WebElement sendButton = driver.findElement(By.id("u_0_s"));
                    sendButton.click();
                }
            }
        }



        driver.quit();
    }
}
